library(data.table)
library(BSgenome.Hsapiens.UCSC.hg19)
library(Biostrings)
hg19 	 <- BSgenome.Hsapiens.UCSC.hg19

chr1 <- unmasked(hg19[['chr1']])

trim_trailing <- function (x) sub("\\s+$", "", x)

read_pwms <- function(fileName)
{
   pwms <- list()
   conn=file(fileName,open="r")
   linn=readLines(conn)
   key <- NULL
   for (i in 1:length(linn)){
      if(grepl(">",linn[i])) {
      	if(!is.null(key)){
      		pwms[[length(pwms)+1]] <- t(df)
      		names(pwms)[[length(pwms)]] <- key
      	}
      	key <- trim_trailing(linn[i])
      	df <- matrix(nrow=0,ncol=4)
      	
      } else {
      	line <- do.call(rbind,strsplit(trim_trailing(linn[i]),split=" "))
      	df <- rbind(df, as.numeric(line[1,2:5])) 	
      }
      
   }
   pwms[[length(pwms)+1]] <- t(df)
   names(pwms)[[length(pwms)]] <- key
   close(conn)

   return(pwms)

   #ic <- c()
   #ic_norm <- c()
   #for (i in 1:length(pwms))
   #{
   #	ic <- c(ic,-sum(pwms[[i]]*log2(pwms[[i]]/back_freq),na.rm=TRUE))
   #	ic_norm <- c(ic_norm,ic[i]/dim(pwms[[i]])[2])
   #	print( ic[i]/dim(pwms[[i]])[2])
   #}
}

read_back_freq <- function(filename)
{
   back_freq <- as.data.frame(fread(filename))
   rownames(back_freq) <- back_freq[, 1]
   back_freq[, 1] <- NULL

   return(back_freq)
}

get_back_freq <- function(back_freq, chr)
{
   chr = paste0("chr", chr)
   if(chr == "chrMT") chr <- "chrM" 

   return(as.numeric(back_freq[chr, 1:4]))
}

read_mutations <- function(fileName)
{
   mutations <- fread(fileName)

   setkey(mutations, FILE, CHROM, POS)

   return(mutations)
}

find_minimum_gap <- function(mutations)
{
   file <- ""
   chr <- ""
   pos <- 0
   min_gap <- -1

   for (i in 1:nrow(mutations))
   {
      if (!identical(file, mutations[i][["FILE"]]) || !identical(chr, mutations[i][["CHROM"]]))
      {
         file <- mutations[i][["FILE"]]
         chr <- mutations[i][["CHROM"]]
      } else {
         if (min_gap == -1)
            min_gap = mutations[i][["POS"]] - pos
         else
         {
            if (min_gap > mutations[i][["POS"]] - pos)
            {
               min_gap <- mutations[i][["POS"]] - pos
               print(paste(mutations[i][["FILE"]], " - ",mutations[i][["CHROM"]], " - ",mutations[i][["POS"]], " - ", pos, " -> ", min_gap))
            }
         }
      }

      pos <- mutations[i][["POS"]]
   }

   return(min_gap)
}


get_close_mutations <- function(mutations, donor, chr, pos, threshold = 27)
{
   # for a given mutation, detect close mutations within its window.
   # return a data.table of mutations
   # example:
   #  > get_window(mutations, "DO45029", "1", 2621471) # have a very close mutation 
   #     FILE CHROM     POS        ID REF ALT            mutation_type probability
   #  1: DO45029     1 2621476 MU2568272   T   C single base substitution    0.276042
   #  > get_window(mutations, "DO45029", "1", 822739) # without any close mutations
   #  Empty data.table (0 rows) of 8 cols: FILE,CHROM,POS,ID,REF,ALT...
   mut_in_chr <- mutations[.(donor, as.character(chr))]
   return(subset(mut_in_chr, POS <= pos +27 & POS >= pos - 27 & POS != pos))  
}

get_window <- function(chr, pos)
{
   # get a window for a mutation
   # the longest motif is 28bp, so the size of window is 28*2 - 1 = 55bp 
   # return the DNA sequence of the window
   # example: 
   #  > get_window(1, 822739)
   #  55-letter "DNAString" instance
   # seq: ATGTAACATCTGCCATTTATAAAATTTCTGTAGAGTTAATAGAACTTTTCCTGAA
   chr = paste0("chr", chr)
   if(chr == "chrMT") chr <- "chrM" 
   seq <- subseq(hg19[[chr]], start = pos - 27, end = pos + 27)
   return(seq)
}

get_potential_motifs <- function(window_seq, pwm)
{
   # This function takes a 55bp window sequence and a position frequency matrix as input
   # return a list of N sequences containing the mutation for a N(bp) motif
   # example:
   # > a = get_window(1, 822739)
   # > get_potential_motifs(a, pwms[[1]]) # note the nucleotide "C" is the mutation here
   # [1] "CTGTAGAG" "TCTGTAGA" "TTCTGTAG" "TTTCTGTA" "ATTTCTGT" "AATTTCTG" "AAATTTCT"
   # [8] "AAAATTTC"
   motif_size <- dim(pwm)[2]
   seqs <- c()
   for (i in (29-motif_size):28)
   {
      seqs <- c(seqs, as.character(subseq(window_seq, start = i, end = i + motif_size - 1)))
   }
   return(rev(seqs))
}

compute_scores <- function(mut, pwm)
{
   # This function takes a mutation (row in a data.frame) and a position frequency matrix
   # as input. It computes the score of all window sequences around the mutation, both of
   # the reference sequence and the mutated sequence. It returns a 2xN matrix, where N is 
   # the number of columns of the PFM. The first row contains the score of the windows in 
   # the reference genome, the second row contains the scores with the mutation present.
   # example:
   # > compute_scores(subs[1,], pwms[[1]])

   pos <- mut[["POS"]]
   window_seq = get_window(mut[["CHROM"]], pos)

   ref_motifs = get_potential_motifs(window_seq, pwm)
   ref_scores = lapply(ref_motifs, score_sequence, pwm=pwm)

   mut_scores <- lapply((1:length(ref_motifs)), function(i) {return(ref_scores[[i]] + score_mutation(mut, pwm, i))})

   return(rbind(ref_scores, mut_scores))
}

score_mutation <- function(mut, pwm, pos)
{
   # Given a mutation, a position frequency matrix and a column index in this matrix,
   # this function computes the relative difference in score caused by the mutation.

   return(pwm[ntoi(mut[["ALT"]]), pos] - pwm[ntoi(mut[["REF"]]), pos])
}

strReverse <- function(x)
        sapply(lapply(strsplit(x, NULL), rev), paste, collapse="")

compute_scores_r <- function(mut, pwm)
{
   pos <- mut[["POS"]]
   window_seq = get_window(mut[["CHROM"]], pos)

   #reverse nucleotide sequences
   ref_motifs <- strReverse(get_potential_motifs(window_seq, pwm))
   #reverse order of scores, so that first score still corresponds to mutation in the first position
   ref_scores <- rev(lapply(ref_motifs, score_sequence, pwm=pwm))

   mut_scores <- lapply((1:length(ref_motifs)), function(i) {return(ref_scores[[i]] + score_mutation_r(mut, pwm, i))})

   return(rbind(ref_scores, mut_scores))
}

score_mutation_r <- function(mut, pwm, pos)
{

   return(pwm[ntoi_r(mut[["ALT"]]), pos] - pwm[ntoi_r(mut[["REF"]]), pos])
}

score_sequence <- function(sequence, pwm)
{
   # This function takes a string and a position frequency matrix as input and computes 
   # the score of the prefix of the sequence corresponding to the number of columns of the 
   # position frequency matrix.
   # example:
   # > score_sequence("GGCAATAT", pwms[[1]])
   # [1] 1.73077

   score <- 0

   for (i in 1:dim(pwm)[2])
   {
      nucleotide <- substr(sequence, start=i, stop=i)
      score <- score + pwm[ntoi(nucleotide), i]
   }

   return(score)
}

ntoi <- function(nucleotide)
{
   # Helper function that converts a character ("nucleotide") into its corresponding row index 
   # in a position frequency matrix

   return(switch(nucleotide, A=1, C=2, G=3, T=4, 0))
}

ntoi_r <- function(nucleotide)
{
   # Helper function that converts a character ("nucleotide") into its corresponding row index 
   # in a position frequency matrix, reversed

   return(switch(nucleotide, A=4, C=3, G=2, T=1, 0))
}

detect_motif <- function(muts, pwm, P_VALUE = 0.99, P_VALUE_2 = 0.99)
{
   #This function takes a list of mutations from the SAME chromosome and a PWM as input and 
   #decides whether the mutation creates or breaks the motif encoded by the PWM.

   #Output:
   #score obtained according to the PWM
   #score obtained according to the PWM with mutation
   #threshold for the given P-value
   #threshold for the given P-value, according to the flanking distribution

   result <- NULL

   N <- dim(pwm)[2]

   back_freq <- get_back_freq(back_freqs, muts[1,][["CHROM"]])
   pwm_log <- log_normalize(pwm, back_freq, 10^2)   

   dis <- score_distribution(pwm, back_freq, 10^2)
   threshold <- threshold(dis, P_VALUE)

   flanking_thresholds <- flanking_thresholds(pwm, P_VALUE_2, back_freq, 10^2)

   thresholds <- rep(threshold, N)

   for (i in 1:nrow(muts))
   {
      scores <- compute_scores(muts[i,], pwm_log)

      for (pos in 1:N)
      {
         flanking_threshold <- flanking_thresholds[[pos]] + pwm_log[ntoi(muts[i,][["ALT"]]), pos]

         if (scores[[1, pos]] >= threshold)
         {
            if (scores[[2, pos]] <= flanking_threshold)
            {
               result <- rbind(result, data.frame(Mutation=muts[i,][["ID"]], Position=pos, Strand="p", Creates="--", REFDiff = scores[[1, pos]] - threshold, ALTDiff = threshold-scores[[2, pos]], ALTFlankDiff = -scores[[2, pos]] + flanking_threshold ))
            } 
            else if(scores[[2, pos]] <= threshold)
            {
               result <- rbind(result, data.frame(Mutation=muts[i,][["ID"]], Position=pos, Strand="p", Creates="-", REFDiff = scores[[1, pos]] - threshold, ALTDiff = threshold-scores[[2, pos]], ALTFlankDiff = -scores[[2, pos]] + flanking_threshold ))
            }
         }
         else 
         {
            if (scores[[2, pos]] >= flanking_threshold)
            {
               result <- rbind(result, data.frame(Mutation=muts[i,][["ID"]], Position=pos, Strand="p", Creates="++", REFDiff = threshold-scores[[1, pos]], ALTDiff = -threshold+scores[[2, pos]], ALTFlankDiff = scores[[2, pos]] - flanking_threshold ))
            }
            else if(scores[[2, pos]] >= threshold)
            {
               result <- rbind(result, data.frame(Mutation=muts[i,][["ID"]], Position=pos, Strand="p", Creates="+", REFDiff = threshold-scores[[1, pos]], ALTDiff = -threshold+scores[[2, pos]] , ALTFlankDiff = scores[[2, pos]] - flanking_threshold))
            }
         }
      }

      scores_r <- compute_scores_r(muts[i,], pwm_log)

      for (pos in 1:N)
      {
         flanking_threshold <- flanking_thresholds[[pos]] + pwm_log[ntoi_r(muts[i,][["ALT"]]), pos]

         if (scores_r[[1, pos]] >= threshold)
         {
            if (scores_r[[2, pos]] <= flanking_threshold)
            {
               result <- rbind(result, data.frame(Mutation=muts[i,][["ID"]], Position=pos, Strand="n", Creates="--", REFDiff = scores_r[[1, pos]] - threshold, ALTDiff = threshold-scores_r[[2, pos]], ALTFlankDiff = -scores_r[[2, pos]] + flanking_threshold ))
            } 
            else if(scores_r[[2, pos]] <= threshold)
            {
               result <- rbind(result, data.frame(Mutation=muts[i,][["ID"]], Position=pos, Strand="n", Creates="-", REFDiff = scores_r[[1, pos]] - threshold, ALTDiff = threshold-scores_r[[2, pos]], ALTFlankDiff = -scores_r[[2, pos]] + flanking_threshold ))
            }
         }
         else 
         {
            if (scores_r[[2, pos]] >= flanking_threshold)
            {
               result <- rbind(result, data.frame(Mutation=muts[i,][["ID"]], Position=pos, Strand="n", Creates="++", REFDiff = threshold-scores_r[[1, pos]], ALTDiff = -threshold+scores_r[[2, pos]], ALTFlankDiff = scores_r[[2, pos]] - flanking_threshold ))
            }
            else if(scores_r[[2, pos]] >= threshold)
            {
               result <- rbind(result, data.frame(Mutation=muts[i,][["ID"]], Position=pos, Strand="n", Creates="+", REFDiff = threshold-scores_r[[1, pos]], ALTDiff = -threshold+scores_r[[2, pos]] , ALTFlankDiff = scores_r[[2, pos]] - flanking_threshold))
            }
         }
      }
   }

   return(result)
}

library(doParallel)
library(foreach)

parallel_by_chromosome<-function(mut, pwm, chroms=c(1:22, "X"), p1=0.9999, p2=0.9999, ncores=4){
   registerDoParallel(cores=ncores)
   output <- foreach(i=1:length(chroms)) %dopar% {
      detect_motif(mut[CHROM==chroms[i]], pwm, p1, p2)
   }
   return(output)
}

pwms <- read_pwms("motif.pfm")
#mutations <- read_mutations("linc_wgs_mutation.tsv")
mutations <- read_mutations("test_mutations.tsv")
back_freqs <- read_back_freq("back_freq.csv")

#subs <- mutations[mutation_type == "single base substitution"]

#do1chr1 <- subset(subs, FILE == "DO45029" & CHROM == "1")



#print(find_minimum_gap(mutations))

#by(do1chr1, 1:nrow(do1chr1), function(row) {
#   print(paste(chr1[row[["POS"]]], " vs. " , row[["REF"]]))
#})




# >NR4A_known1 NR4A2#known NR4A2_jaspar_MA0160.1 +
# R 0.615385 0.076923 0.230769 0.076923
# A 0.928571 0.000000 0.071429 0.000000
# G 0.000000 0.000000 0.928571 0.071429
# R 0.214286 0.000000 0.785714 0.000000
# T 0.142857 0.142857 0.000000 0.714286
# C 0.000000 0.928571 0.000000 0.071429
# A 1.000000 0.000000 0.000000 0.000000
# M 0.230769 0.615385 0.153846 0.000000