library(data.table)

score_distribution <- function(pwm, b_freq, precision = 10 ^ 3)
{
	pwm_log <- log_normalize(pwm, b_freq, precision)

	n <- dim(pwm)[2]
	steps <- precision * n

	s_dist <- setClass("s_dist", slots = c(min_score="numeric", 
				max_score="numeric", 
				score_diff="numeric", 
				motif_density="numeric",
				motif_cdf="numeric"))

	d <- s_dist(min_score=min_score(pwm_log), max_score=max_score(pwm_log), score_diff=0, motif_density=rep(0, steps))

	d@score_diff <- (d@max_score - d@min_score) / (steps - 1)

	d@motif_density[diff_to_index(0 - d@min_score, d@score_diff) + 1] <- 1.0

	for (i in 1:n)
	{
		dens <- rep(0, steps)

		for (c in 1:4)
		{
			for (p in 1:steps)
			{
				idx <- wrap_index(p + diff_to_index(pwm_log[c, i], d@score_diff), steps)
				#dens[idx] <- dens[idx] + d@motif_density[p] * pwm[c, i]
				dens[idx] <- dens[idx] + d@motif_density[p] * b_freq[c] #compute probability of a false positive
			}
		}

		d@motif_density <- dens
	}

	d@motif_cdf <- cumsum(d@motif_density)

	return(d)
}

threshold <- function(d, p)
{
	# Using the precomputed s_dist structure, this function finds the score leading to 
	# a certain p-value by binary search.

	return(d@min_score + d@score_diff * (findInterval(p, d@motif_cdf) - 1))
}

wrap_index <- function(idx, steps)
{
	return(max(1, min(steps, idx)))
}

diff_to_index <- function(x, score_diff)
{
	return(round(x / score_diff))
}

log_normalize <- function(pwm, b_freq, precision = 10 ^ 3)
{
	# Takes a PWM and the background frequency of nucleotides and computes
	# the log-normalized PWM matrix, using pseudocounts.

	#add a small value to all cells to make them positive and scale back
	pwm <- (pwm + (1/precision)) / (1 + 4/precision)

 	return(log2(pwm / b_freq))
}

min_score <- function(pwm)
{
	# Returns the minimal score a sequence can score with respect to this PWM

	return(min(0, sum(do.call(pmin, c(data.frame(t(pwm)), na.rm=TRUE)))))
}

max_score <- function(pwm)
{
	# Returns the maximal score a sequence can score with respect to this PWM

	return(max(0, sum(do.call(pmax, c(data.frame(t(pwm)), na.rm=TRUE)))))
}

information_content <- function(pwm)
{
	# Computes the information content (in bits) of the given PWM
   
    return(2*dim(pwm)[2] + sum(pwm*log2(pwm),na.rm=TRUE))
}

flanking_thresholds <- function(pwm, p_value, b_freq, precision = 10 ^ 3)
{
	# Given a motif of length N and a P-value, this function computes 
	# the corresponding scores for every subsequence of N-1 nucleotides.
	# The first nuclotide it leaves out is the last one, the last is the first
	# one. Compare read_pfm.r/get_potential_motifs
 
	thresholds <- c()

	N <- dim(pwm)[2]

	for (i in 1:dim(pwm)[2])
	{
		sub_pwm = pwm[,-i]

		distribution = score_distribution(sub_pwm, b_freq, precision)

		thresholds <- c(thresholds, threshold(distribution, p_value))
	}

	return(thresholds)
}


flanking_distributions <- function(pwm, b_freq, precision = 10 ^ 3)
{

	distributions <- c()

	N <- dim(pwm)[2]

	for (i in 1:dim(pwm)[2])
	{
		sub_pwm = pwm[,-i]

		distributions <- c(distributions,score_distribution(sub_pwm, b_freq, precision))

	}

	return(distributions)
}