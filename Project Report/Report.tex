\documentclass[11pt]{article}
\usepackage{graphicx}
\usepackage{float}
\usepackage{caption}
\usepackage{subcaption}
\graphicspath{ {plots/} }

\usepackage{amsmath, amssymb, amsthm}
\usepackage{mathtools}
\usepackage{hyperref}
%\usepackage[margin=2.5cm]{geometry}
\usepackage{todonotes}

\usepackage{apacite}

\theoremstyle{definition}
\newtheorem{claim}{Claim}
\newtheorem{fact}{Fact}

\newtheorem{definition}{Definition}
\theoremstyle{plain}
\newtheorem{lemma}{Lemma}
\newtheorem{coro}{Corollary}
\newtheorem{conj}{Conjecture}

\newcommand*{\N}{\mathbb{N}}
\newcommand*{\Z}{\mathbb{Z}}
\newcommand*{\F}{\mathbb{F}}
\newcommand*{\R}{\mathbb{R}}
\newcommand*{\Q}{\mathbb{Q}}
\newcommand*{\C}{\mathbb{C}}
\renewcommand*{\Pr}{\mathrm{\bf P}}
\newcommand*{\E}{\mathrm{\bf E}}

\newcommand{\defeq}{\vcentcolon=}
\newcommand{\eqdef}{=\vcentcolon}

\makeatletter
\newcommand*{\rom}[1]{\expandafter\@slowromancap\romannumeral #1@}
\makeatother


\setlength{\parindent}{0cm}

\usepackage[utf8]{inputenc}
\title{Motif Detection Under the Presence of Mutations}
\author{Fabian Henneke, Shimin Shuai}
\date{CSC2417H -- Project Report}
\begin{document}
\maketitle
\section{Abstract}
Germ-line or somatic mutations in the human genome have fundamental roles in many human diseases. Most of mutations are in non-coding regions. One potential molecular mechanism of non-coding mutations is that a sequence motif may be created or disrupted by the mutation, such as somatic mutations in \textit{TERT} promoters or the SNP(rs6983267) in \textit{MYC} enhancers.  We describe the process of specifically detecting such events, given a list of mutations and motifs. As the procedure deciding whether there the motif matches fundamentally depends on meaningful threshold values, we propose a new statistical measure from which thresholds can be derived. We derive it from the distribution of nucleotides in the "flank" of the mutation and demonstrate that it can be computed efficiently.

\section{Biological Background}

The vast majority (97\%) of the human genome is not coding for proteins and around 80\% of the human genome participates in at least one biochemical process  \cite{RefWorks:doc:54f8bb1ae4b018e5cfff9064}. Genomic variation (inherited or somatic) has a critical role in human disease  \cite{RefWorks:doc:5542d6c6e4b0d793db4a1344}. For inherited genetic variation, according to the 1000 Genomes Project, the average number of single nucleotide polymorphisms (SNPs) per person is about 3.6 million (0.1\% of the human genome)  \cite{RefWorks:doc:55426ec8e4b0d793db4a0d6d}. Although genome-wide association studies (GWAS) have successfully linked some genotypes to phenotypes statistically, most of GWAS identified loci (about 88\%) are within non-coding regions and lack of biological explanations  \cite{RefWorks:doc:54f8bb1ae4b018e5cfff9064}. Besides inherited genetic variation, somatic genetic variation is also known to cause diseases like cancer  \cite{RefWorks:doc:54f8a17fe4b02fa3dbc56e82}. Large-scale cancer genome sequencing projects such as the Cancer Genome Atlas (TCGA) and the International Cancer Genome Consortium (ICGC) provide us with huge amounts of somatic mutation profile data for different cancer types. Similar to inherited mutations, although most of somatic mutations are in non-coding regions, almost all cancer driver mutations, mutations making contributions to tumorigenesis, identified so far are in protein-coding genes such as \textit{TP53} and \textit{KRAS}  \cite{RefWorks:doc:54f8a1a2e4b003a94110e121, RefWorks:doc:54f8babde4b018e5cfff9061}. The interpretation of inherited and somatic variations in non-coding regions is one of the major topics in human genomics at present  \cite{RefWorks:doc:54f8bb90e4b018e5cfff907b, RefWorks:doc:54f8bb7ce4b018e5cfff9077, RefWorks:doc:54f8bb73e4b0641b1a6f9b23}.  

One known molecular mechanism for non-coding mutations is that a sequence motif may be created or disrupted by the mutation \cite{RefWorks:doc:5506ea0ce4b034955b5c6893}. Sequence motifs are short, recurring patterns in DNA that are presumed to be bound by sequence-specific DNA binding proteins like nucleases and transcription factors (TF) \cite{RefWorks:doc:55424244e4b0d793db4a0af8}. For example, somatic mutations in the promoter region of telomerase reverse transcriptase (\textit{TERT}) were found highly recurrently in bladder cancer (59\%), skin cancer (29\%) and liver cancer (40\%) and so on \cite{RefWorks:doc:54f8bbf1e4b018e5cfff907e}. Those mutations found in \textit{TERT} promoters created new DNA motifs for ETS family transcription factors (Figure \ref{fig:tert}) and the \textit{TERT} gene expression level was therefore increased in cancer tissues. 

\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{tert}
\caption{Schematic figure of the \textit{TERT} promoter region. (Modified from Koelsche \textit{et al.}, 2014)} \label{fig:tert}
\end{figure}

Another example is a inherited variant, rs6983267 on 8q24. This SNP significantly associated with cancer pathogenesis and it's located within the enhancer of the \textit{MYC} proto-oncogene. The allele T at this position is found to decrease the binding affinity for transcription factor 7-like 2 (TCF7L2) and induce long-range chromatin interaction with \textit{MYC} in colorectal cancer \cite{RefWorks:doc:5506fd5de4b034955b5c6a5e, RefWorks:doc:5506f614e4b034955b5c69c1}.

Motivated by the discovery of the somatic \textit{TERT} promoter mutation and the inherited \textit{MYC} enhancer mutation, we would like to identify more such mutations, which when presenting, are able to create or break certain DNA sequence motifs. 

\section{Algorithmic Description}

\subsection{Motif Detection in Individual Sequences and Sequences with Mutations}

From an algorithmic perspective, a \emph{nucleotide sequence} is a string $G$ of letters drawn from a finite alphabet $\Sigma$, e.g. $\Sigma=\{A, C, G, T\}$. As e.g. the DNA is double-stranded, to every such sequence belongs a dual sequence, which is obtained from $G$ by reversing the sequence and substituting $A\leftrightarrow T, C\leftrightarrow G$. We will denote this sequence by $\overline G$. A \emph{motif} of length $N$ is usually given as a so-called Position Weight Matrix (PWM), which is a $|\Sigma| \times N$-matrix of non-negative real numbers (the concept goes back to  \cite{Stormo11051982}). The entries in any column $i$ add up to one and express the probability that a nucleotide sequence exhibiting the motif has a certain nucleotide at position $i$ relative to the beginning of the motif match. A \emph{consensus sequence} can be obtained by selecting from every column the letter in $\Sigma$ corresponding to the largest entry. This sequence is the most likely instance of the motif to occur and can be used a human-readable description of the motif.

The process of detecting the presence of a motif in an individual sequence $G$ proceeds in two steps. In the first, the PWM $M$ will be preprocessed into another matrix $\tilde M$ having the same dimensions. In the second, the matrix $\tilde M$ will be used to assign a score to every position in $G$, which is then subject to further interpretation. As this process will be an important subroutine when working with a sequence with mutations, we will describe how we concretely implement these steps. We use two additional parameters: a precision threshold $\epsilon_0 >0$ and a vector $b\in (0,1)^\Sigma$ containing the background  frequency of the nucleotides, which will most often consist of the frequencies with which the letters of $\Sigma$ appear in $G$. Using this additional information, the preprocessing step breaks up into the following parts:
\begin{enumerate}
\item In order to avoid zero values in $M$, add $\epsilon_0$ to every entry of $M$ and then scale every entry by $\frac{1}{1+|\Sigma|\epsilon_0}$
\item Divide every row corresponding to a letter $\sigma\in \Sigma$ by $b[\sigma]$.
\item Obtain the matrix $\tilde M$ by taking the (natural) logarithm
\end{enumerate}
This process is called \emph{log normalization} and converts $M$ into a form where every entry represents the deviation from a  random distribution according to the background frequency. The advantage using the logarithm of the normalized entries is that it allows to compute the score value in the next step using only additive operations.

After the matrix $\tilde M$ has been computed, a score indicating how close the motif matches the sequence $G$ at position $k$ can be computed in time linear in the length $N$ of the motif. The score is given by
\[s(G, k, M)=\sum_{i=0}^{N-1} \tilde M[G[k+i], k+i].\]
The computed value does not have a unit and can also not be interpreted as a probability. We will show how to interpret it in the next section. For now we assume that we are provided a score threshold $T$ and we say that $G$ exhibits the motif $M$ at position $i$ if $s(G, k, M)\geq T$.

If we assume that the threshold value $T$ is determined in a meaningful way, the process above gives a generic method to detect motifs in individual sequences. Going a step further, we now want to detect motif in the context of a nucleotide sequence with mutations. We assume that the only type of mutations occuring is \emph{single nucleotide substitution}, where other imaginable types would comprise (multi-nucleotide) insertions and deletions. The latter affect the local form of the sequence more significantly and their influence on the presence of motifs can therefore be more complicated to analyze. We further assume that we are provided with a list of mutations, where a single mutation is given by the position of the nucleotide affected, the original nucleotide and the one it is changed to. Given a sequence and such a list of mutations, we can detect motifs that might be affected the mutations in the following way:
\begin{enumerate}
\item Sort the mutations by position and search for pairs of motifs that are closer than the maximal length of a motif under consideration, denoted by  $N_{\max}$. (The treatment of such mutations is not much more difficult than the case of individual mutations, but for ease of presentation we will assume that the gap between two successive mutations is larger than the maximal length of a motif.)
\item For every mutation $\sigma\to\sigma'$, retrieve a window of length $2N_{\max} -1$ centered at its position form the nucleotide sequence. (This step is only necessary if access to the  sequence is slow, e.g. because it is a large portion of a genome and resides in memory that is slow to read from if not done sequentially.)
\item For every motif $M$ of length $N$:
\begin{enumerate}
\item Generate all length $N$ substrings of the window sequence that contain the mutated nucleotide. (There are $N$ such substrings in total, which we denote by $S_1, \dots, S_N$, with the index indicating in which position of the substring the mutation occurs.)
\item Compute $s_{\text{ref}}[i]=s(S_i, 1, M)$ for every $i=1, \dots, N$. (This is the score of the reference sequence.)
\item Compute $s_{\text{alt}}[i]=s_{\text{ref}}[i]+ M[\sigma', i] - M[\sigma, i]$ for every $i=1, \dots, N$. (This is the score of the altered sequence, for which we only have to compute the change in score at the position where the mutation occurs.)
\item Repeat these computations for the other strand. For example, we can reverse the order of the columns and rows of $M$, obtaining a matrix $M'$. Then  $\overline s_{\text{ref}}[i]=s(\overline S_i, 1, M)=s(S_i, 1, M')$, i.e. we do not have to use potentially slow string operations on the $S_i$ and can directly manipulate the log normalized matrix $\tilde M$.
\item For every position $i$, compare both $s_{\text{ref}}[i]$ and $s_{\text{alt}}[i]$ resp. $\overline s_{\text{ref}}[i]$ and $\overline s_{\text{alt}}[i]$ to threshold values and decide whether the motif occurs in the reference or altered sequence.
\end{enumerate} 
\end{enumerate}
All but the last step can easily be implemented and we will now focus on ways to make the last step more concrete. When dealing with motifs in the presence of mutations, the important question is whether a given mutation \emph{creates} or \emph{breaks} a motif and whether this has certain biological consequences. This question is obviously too vague to be answered by algorithmic methods only, but the aim of the following sections will be to study statistical measures that could be seen to at least correlate with biological significance. For this, we will describe possible definitions of \emph{motif creation} that can be checked algorithmically. Adhering to the scoring framework using PWMs presented above, we will mostly be interested in determining meaningful and robust threshold values.

\subsection{P-Value Computation by Dynamic Programming}
Similar to the structure of the previous section, we will first review methods to compute score thresholds for individual sequences in a way that allows us to then generalize them to sequences with mutations. The most important information for the computation of meaningful thresholds is the background frequency of every nucleotide. The process of detecting a motif is equivalent to testing the null hypothesis that a certain part of the sequence is governed by the background distribution instead of the distribution indicated by the motif's PWM. To measure the statistical significance of our decision, we have to obtain estimates for the probability that we mistakenly accept or refute this hypothesis. A common approach in the field of motif detection is to set a threshold as to limit the probability of a "false positive", i.e. the case in which we decide that some part of the sequence exhibits the motif although it does not. As it is very common for nucleotide sequences to be very long, we prefer to minimize the likelihood of this error and minimize the number of positions at which we have conduct further experiments.

If the distribution of nucleotides in the motif does not differ from the background distribution, the log normalized score will always be 0. A higher score indicates that the part of the sequence is further away from being random. Given a PWM $M$ representing a motif of length $N$ and the corresponding log normalized matrix $\tilde M$, we define a function 
\[P_M(t)=\Pr[s(X, 1, M) \leq t].\]
Here $X$ is a random nucleotide sequence, i.e. a sequence of independent random letters from the alphabet $\Sigma$, where each letter $\sigma$ is drawn with probability equal to $b(\sigma)$. $P_M$ is the cumulative distribution function of the random variable $s(X, 1, M)$, the score of a random nucleotide sequence with the same nucleotide frequency as $G$. $P_M(t)$ takes values in $[0, 1]$ and is constant below the minimum and above the maximum score. As there are only finitely many possible nucleotide sequences of a given length, the distribution is discrete and $P_M$ is only piecewise continuous.

\begin{figure}[H]
\centering
\begin{subfigure}{0.45\textwidth}
\centering
\includegraphics[width=\linewidth]{score_d_pmf.png}
\caption{Density of the distribution $P_M(t)$ for $M$ used in the example given in \ref{testrun}}
\end{subfigure}
\begin{subfigure}{0.45\textwidth}
\centering
\includegraphics[width=\linewidth]{score_d_cdf.png}
\caption{Corresponding cumulative distribution function $P_M(t)$}
\end{subfigure}
\end{figure}

Knowledge about the CDF $P_M(t)$ now enables us to compute a threshold for motif detection in a statistically meaningful way: Given a so-called \emph{P-value} $p\in[0, 1]$, we can find the smallest value $T$ such that $P_M(T)\geq 1-p$ by binary search. We then know that at most a $p$-fraction of all random nucleotide sequences would exhibit the motif, i.e. we are certain to the $100p\%$-level of significance that motifs scoring higher than $T$ are not false positives (type \rom{1} error). In order to obtain the threshold corresponding to the given P-value, we have to compute the distribution function $P_M(t)$. In general, transforming P-values into score thresholds and vice versa is an NP-hard problem  \cite{18072973}. The reduction used in the proof of this hardness result is from \textsc{SUBSET SUM}, so it could be expected that the problem has a solution in pseudo-polynomial time. As the entries of the matrix $\tilde M$ will most often not be rational, we have to apply some rounding procedure. We will denote the minimum resp. maximum score achievable by any sequence with respect to $\tilde M$ by $s_{\text{min}}$ resp. $s_{\text{max}}$ and the difference between the two by $D\coloneqq s_{\text{max}}-s_{\text{min}}$. If we are willing to round the a priori not rational entries of the matrix $M$ by at most $\epsilon$, then the following dynamic programming algorithm computes the discrete distribution $P_M(t)$ in time $O(\frac{D}{\epsilon} |\Sigma| N)$:
\begin{enumerate}
\item Compute $s_{\text{min}}$ and $s_{\text{max}}$ by summing the minimum resp. maximum values in each column of $\tilde M$. (We can assume that $s_{\text{min}}<0$ and $s_{\text{max}}>0$, otherwise no or every sequence would be more likely to exhibit the motif than the background distribution, which can only happen in pathological cases.)
\item Create an array $d[i]$ with entries $i=1, \dots, \frac{D}{\epsilon}$ and initialize all entries to 0. Set
\[d\left[\left\lfloor \frac{-s_{\text{min}}}{\epsilon}\right\rfloor\right]\coloneqq 1.\]
(The entry $\left\lfloor -s_{\text{min}}/\epsilon\right\rfloor$ corresponds to a score of 0, which is attained with probability 1 by an empty PWM.)
\item For every $k=1,\dots, N$:
\begin{enumerate}
\item Make a temporary copy $d'$ of $d$.
\item For every $\sigma\in\Sigma$, $i=1, \dots \frac{D}{\epsilon}$:
\begin{enumerate}
\item Compute $l\coloneqq i+\left\lfloor\tilde M[\sigma, k]/\epsilon \right\rfloor$ and cut if off if it lies outside the interval $[1, \frac{D}{\epsilon}]$. ($l$ is $i$ plus the index difference between two entries in $d$ that are a score value of $M[\sigma, k]$ apart.)
\item Set $d'[l]\coloneqq d'[l] + d[i] \cdot b[\sigma]$. (This updates the score distribution to also include the possibility that the sequence has a $\sigma$ in position $k$, with a score of $\approx i\epsilon + s_{\text{min}}$ on the first $k-1$ positions.)
\end{enumerate}
\item Set $d\coloneqq d'$ and discard $d'$.
\end{enumerate}
\item Compute the cumulative sum over the array $d$, which we also denote by $d$.
\end{enumerate}
The end result of this algorithm is the array $d$. The value $d[i]$ gives the probability that the score attained by a random nucleotide sequence is at most $i\cdot\epsilon+ s_{\text{min}}$, so the array $d$ contains all information on $P_M$. Using binary search on the array we can now compute threshold values in time $\log \frac{D}{\epsilon}$.

Using this algorithm, there is already a simple criterion for a mutation creating or breaking a motif that we can check: Given a P-value $p$, we compute the corresponding threshold $T$. If $s_{\text{ref}}[i]>T$ and $s_{\text{alt}}[i]<T$ in step 3.(e) of the algorithm from the previous section, we decide that the mutation created the motif. This decision procedure does not take into account the difference in score the mutation causes. Even an infinitesimal change around the threshold would cause it to decide that a motif has been created or broken. The method is also sensitive to mutations in positions where the values in the corresponding column of $\tilde M$ vary strongly. If the difference in score in this position is large enough, the sequence might surpass the threshold although the nucleotides at other positions are not in good agreement with the motif.

\subsection{The Flanking Thresholds}
In this section, we propose a new statistical measure that can be used to obtain thresholds for motif detection under the presence of mutations. It addresses some of the problems described at the end of the preceding section, while still being efficiently computable. Although it requires the computation of not only one but $N+1$ probability distributions, we will demonstrate how to compute them all simultaneously with only logarithmic overhead.

\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{flank_d_pmf.png}
\caption{Density of the distribution $P_M^{i,T}(t)$ for $M$ used in the example given in \ref{testrun} and $i=6, 10, 13$}
\end{figure}

In the last section, the thresholds were derived from the distribution $P_M(t)$, which was independent of the mutation. We will now consider the family of distributions 
\[P_M^{i,\sigma}(t)=\Pr[s(X, 1, M) \leq t \mid X[i]=\sigma ],\]
where $i=1, \dots, N, \sigma\in \Sigma$ and $X$ is a random nucleotide sequence of length $N$ according to the background distribution. These distributions are similar to $P_M$ in that they also yield the probability that a random sequence surpasses a given threshold, the difference being that this probability is taken conditional on fixing a single nucleotide in the sequence. The motivation for this is that under the presence of a mutation $\sigma\to\sigma'$ at position $k$, the distribution $P_M^{k, \sigma'}$ describes the scoring behaviour of a random sequences that exhibits this mutation. We call these distributions \emph{flanking distributions}, as they are determined by the flank of the mutation. If $\sigma'$ is less likely than $\sigma$ to be part of the motif $M$ at position $k$, the CDF $P_M^{k, \sigma'}$ will be shifted to the left compared to $P_M$ and vice versa. This means that if we were to compute a threshold value, called the \emph{flanking threshold}, using the same P-value, it will turn out lower resp. higher than the threshold computed from $P_M$. By fixing the effect of the mutation on the score, the flanking distributions allow to restrict attention to occurences of created or broken motifs that are supported by the rest of the nucleotides and not just strongly influenced by the mutation itself. 



In order for the flanking distribution to be a useful tool in the detection of motifs, it has to be easily computable. The definition above would require us to compute $|\Sigma| N$ different probability distributions. This can easily be brought down to only $N$ distributions by using that
\[P_M^{i,\sigma}(t)=\Pr[s(X[1,\dots, i-1] + X[i+1, \dots, N], 1, M_i) \leq t + M[\sigma, i]],\]
where $X[1,\dots, i-1] + X[i+1, \dots, N]$ is the string $X$ without the $i$-th letter and $M_i$ is obtained from the matrix $M$ by deleting the $i$-th column. This relation holds as the score assigned to the position $i$ of $X$ is fixed and all the nucleotides at other positions are independent of it. From this we can also see that we can reuse the algorithm presented in the last section to compute the functions $P_M^{i,\sigma}(t)$: The RHS of the equation above is equivalent to a shifted version of the distribution $P_{M_i}$ and can be obtained in constant time from this one. The problem of computing all $N$ flanking distributions -- and the associated thresholds -- can therefore be solved in time $O(\frac{D}{\epsilon} |\Sigma| N^2)$. 


\subsection{An $O(\frac{D}{\epsilon} |\Sigma| N \log N)$ Algorithm for the Flanking Thresholds}
As a conclusion to our discussion of the algorithmic aspects of the flanking distributions, we propose a faster algorithm for their computation. It is based on the DP algorithm reviewed in section 3.2, but differs in step 3 of this algorithm. The main idea is that the order in which the columns of $M$ are processed does not matter: The steps $3 (a)-(c)$ for different values of $k$ commute with each other. We denote the $i$-th column of $M$ by $c_i$ and the trivial probability distribution $d$ in which score 0 is obtained with probability 1 by $0$. If we then write $\circ$ for the operation of adding the scoring information contained in that row to $d$ by executing step $3$ for the value $k=i$, then we can describe the computation of the entire score distribution schematically as $0\circ c_1\circ c_2\circ \dots\circ c_N$. The $N$ flanking distributions $P_{M_i}$ can be obtained as $0\circ c_1\circ c_2\circ \dots\circ\hat c_i\circ\dots\circ c_N$, where $\hat c_i$ indicates that this column is left out. 

A divide-and-conquer approach now allows us to carry out these computations in parallel. For ease of presentation, we assume $N$ to be a power of 2. This can always be achieved by padding the matrix $\tilde M$ with zero columns. 

\begin{enumerate}
\item Set $L_0\coloneqq 0, R_0\coloneqq 0$ The initial set of remaining columns is $c_1, \dots, c_N$, the initial distribution is $D\coloneqq 0$.
\item Run the following recursive subroutine:
\begin{enumerate}
\item Given a set $c_i, \dots, c_j$ of columns and a distribution $D$, compute the two distributions $L\coloneqq D\circ c_i\circ c_{i+1}\circ \dots\circ c_{\frac{i+j}{2}}$ and $R\coloneqq D\circ c_{\frac{i+j}{2}+1}\circ c_{\frac{i+j}{2}+2}\circ \dots\circ c_{j}$.
\item If $j=i+1$, stop and output $L$ and $R$.
\item Otherwise, recursively call this subroutine on two sets of input: One receives as parameters the columns $c_i,\dots, c_{\frac{i+j}{2}}$ and the distribution $R$, the other one receives the columns $c_{\frac{i+j}{2}+1},\dots, c_{j}$ and the distribution $L$.
\end{enumerate}
\item Collect the output from all subroutines in order. If the call to the $L$ branch is always performed before the call to the $R$ branch, the output will consist of the $N$ flanking distributions in reverse order.
\end{enumerate}
On every depth of recursion, every column is added to exactly one distribution. As the size of the set of remaining columns decreases by a factor of two in every recursive call, the depth of the recursive computation is $\log_2 N$. All in all, the total number of $\circ$  operations is hence of the order of $O(N \log N)$. As every $\circ$ operation takes time $\frac{D}{\epsilon} |\Sigma|$, the total running time of this algorithm is $O( \frac{D}{\epsilon} |\Sigma| N\log N)$. The number of temporary distributions in memory is linear in $N$.
\section{Experiments \& Results}
We implemented the computation of the flanking and score distributions as described in the previous chapters. Our program detects both the creation and the disruption of a motif. It assigns a quality measure to such an event: A "+" or "-" indicates that a motif has been created or broken by the definition using only the classical score threshold. A "++" or "--" indicates that a motif has been created or broken by also surpassing the respective flanking threshold.
\subsection{Test run with program output}
\label{testrun}
The following example shows how our program determines that a mutation $G\to T$ breaks a TCF7 motif (Figure \ref{fig:tcf7}) present in chromosome 8. The motif was present on the positive strand (Strand: "p") and its score with mutation present is even below the flanking threshold (Creates: "-{}-" instead of just "-"). Without the mutation, the score surpasses the threshold for a P-value of $10^{-4}$ by $\approx 0.366$ (REFDiff). With the mutations, the score drops to $\approx 2.58$ below the threshold (ALTDiff) and $\approx 0.978$ below the flanking threshold (ALTFlankDiff), which also stems from a P-value of $10^{-4}$.

\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{tcf7}
\caption{The sequence Logo of the TCF7L2 transcription factor.} \label{fig:tcf7}
\end{figure}

\begin{verbatim}
> pwms[[10]]
         [,1]     [,2]     [,3]     [,4]     [,5]     [,6]     [,7]     [,8]
[1,] 0.172207 0.394463 0.252997 0.593736 0.034749 0.887341 0.047253 0.031190
[2,] 0.233961 0.230985 0.220534 0.041281 0.353059 0.002094 0.002581 0.926187
[3,] 0.243129 0.141480 0.167597 0.142360 0.567994 0.001362 0.000760 0.031751
[4,] 0.350703 0.233072 0.358872 0.222623 0.044198 0.109203 0.949406 0.010872
         [,9]    [,10]    [,11]    [,12]    [,13]    [,14]    [,15]    [,16]
[1,] 0.967624 0.982678 0.958680 0.074703 0.199517 0.439156 0.346074 0.307665
[2,] 0.001379 0.001941 0.000940 0.034150 0.180529 0.150903 0.254018 0.258178
[3,] 0.001363 0.008215 0.023834 0.880320 0.526578 0.333901 0.161120 0.157893
[4,] 0.029634 0.007166 0.016546 0.010827 0.093376 0.076041 0.238788 0.276264
        [,17]
[1,] 0.272277
[2,] 0.254855
[3,] 0.187143
[4,] 0.285725
> mutations[2,]
   FILE CHROM       POS     ID REF ALT             FLANK         NOTE
1: test     8 128413305 TCF7L2   G   T TGAAAG[G/T]CACTGA MYC_enhancer
               SOURCE
1: NatureGenetics2009
> detect_motif(mutations[2,], pwms[[10]], 0.9999, 0.9999)
  Mutation Position Strand Creates   REFDiff  ALTDiff ALTFlankDiff
1   TCF7L2       13      p      -- 0.3661161 2.583332    0.9775675
\end{verbatim}
This result for TCF7L2 motif is consistent with previous experimental results, which showed that TCF7L2 transcription factor complex binds preferentially to the G allele in colon cancer cells \cite{RefWorks:doc:5506fd5de4b034955b5c6a5e}. 

\subsection{Test for \textit{SDHD} promoter mutations}

Weinhold \textit{et al.} reported three mutations in \textit{SDHD} promoter break ETS transcription factor binding sites in melanoma cancer, chr11:111,957,523(C$>$T), chr11:111,957,541(C$>$T) and chr11:111,957,574 (C$>$T) (Figure \ref{fig:sdhd})  \cite{RefWorks:doc:54f8bb73e4b0641b1a6f9b23}.

\begin{figure}[H]
\centering
\includegraphics[scale=0.44]{sdhd}
\caption{Mutations in \textit{SDHD} promoters. (Modified from Weinhold \textit{et al.}, 2014)} \label{fig:sdhd}
\end{figure}

We collected all known PWMs for ETS family transcription factors (22 in total) and then found motifs with those three mutations. However, our results showed that only one mutation,  chr11:111,957,523(C$>$T), showed signal for motif disruption ( "-", REFDiff $\approx$ 6.81, ALTDiff $\approx$ 0.33 and ALTFlankDiff $\approx$ -3.92). Our explanation for that is the representation of motifs used by us and Weinhold \textit{et al.} are different. Weinhold \textit{et al.} used a consensus  four nucleotide sequence "TTCC" to represent ETS family transcription factors, while we used several longer probability representation, the PWMs. Using a short consensus sequence might overestimate the number of motif-creation or -disruption events.


\subsection{Test using an ICGC liver cancer dataset}

We also test our method using a liver cancer dataset. This dataset includes 354,201 unique single nucleotide substitutions obtained from whole genome sequencing of 31 donors. 

Since 12 donors (38.7\%) have known \textit{TERT} promoter mutations in their genome, we first use all mutations to search against the ETS motif (Figure \ref{fig:ets}). Among all 354,201 unique mutations, 2,942 mutations (0.83\%) are reported by our program to be associated with the ETS motif. The \textit{TERT} promoter mutation (chr5:1295228, G$>$A) is in the list with "++", REFDiff $\approx$ 0.31, ALTDiff $\approx$ 5.74 and ALTFlankDiff $\approx$ 2.12.

\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{ets}
\caption{The sequence logo of the ETS transcription factor.} \label{fig:ets}
\end{figure}

Next, we perform the same search incorporating genomic annotations. We search ETS motif in promoter regions based on GENCODE annotation version 17 \cite{RefWorks:doc:54f8c6c5e4b018e5cfff915a}. There are in total 8,662 unique mutations in promoter regions, 143 of them are associated with the ETS motif (1.65\%). We noticed that the ratio of motif-associated mutations is elevated (1.65\% to 0.83\%) in promoter regions than the whole genome, which seems biologically plausible since real binding sites of ETS transcription factor are more likely to be in promoters.


\section{Outlook}
In order to better understand the correlation between the biological significance of a mutation and the statistical significance to which it is detected to be present in a nucleotide sequence, more experiments would have to be conducted. Further data information on mutations known to create or disrupt biological functionality could be used to evaluate the meaningfulness of the flanking thresholds and finetune the parameters, e.g. the P-values. One could also try to make their computation more efficient. This is most likely not possible asymptotically, but heuristics already in use to speed up the normal P-value transformations could be adapted to the computation of flanking thresholds.

\bibliographystyle{apacite}
\bibliography{ref_shimin}
\end{document}
